let documentacao = document.querySelector('.leitura')
if(documentacao){
    let listDocumentacao = document.querySelector('.list-doc')

    documentacao.addEventListener('click', function(){
        listDocumentacao.classList.toggle('active')      
    })
}


/** nav bar */
let itensNav = Array.from(document.querySelectorAll('.iten-nav'))
let listaPlanos = Array.from(document.querySelectorAll('.planos-uni'))

function removePlanos(item){
    if(item){
        itensNav.forEach(function(itemNav){
            if(item != itemNav){
                itemNav.classList.remove('active')
            }
        })
    }

    if(listaPlanos){
        listaPlanos.forEach(function(item){
            item.classList.remove('active')
        })
    }

    
}
function activePlanos(planos){
    let itens = Array.from(planos)

    if(itens){
        itens.forEach(function(item){
            item.classList.add('active')
        })
    }
}

itensNav.forEach(function(item){
    item.addEventListener('click', function(e){
        let id = item.dataset.id
        
        item.classList.add('active')

        let itens = document.querySelectorAll('#'+ id)

        removePlanos(item)

        activePlanos(itens)
    })
})