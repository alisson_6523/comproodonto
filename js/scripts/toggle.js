let toggle = document.querySelector('.toggle-mobile')
let menu = document.querySelector('.modal-menu')
let closeMenu = document.querySelector('.close-menu')

menu.addEventListener('click', function(e){
    if(e.target.classList[0] == 'modal-menu'){
        menu.classList.toggle('active')   
    }
})


toggle.addEventListener('click', function(){
    menu.classList.toggle('active')
})

closeMenu.addEventListener('click', function(){
    menu.classList.toggle('active')    
})