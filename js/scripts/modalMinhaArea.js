let btnOpenModal = Array.from(document.querySelectorAll('.btn-add'))
let modalAddPlano = document.querySelector('.modal-add-plano') 
let closeModalAdd = document.querySelector('.close-modal-add') 
let openModalBtnBeneficio = document.querySelector('.add-beneficio') 
let meusDadosAlterar = Array.from(document.querySelectorAll('#alterar'))


modalAddPlano.addEventListener('click', function(e){
    if(e.target.classList[0] == 'modal-add-plano'){
        modalAddPlano.classList.toggle('active')
    }
})

meusDadosAlterar.forEach(function(alterar){
    alterar.addEventListener('click', function(e){
        modalAddPlano.classList.toggle('active')           
    })
})

openModalBtnBeneficio.addEventListener('click', function(e){
    modalAddPlano.classList.toggle('active')
})

btnOpenModal.forEach(function(btn){
    btn.addEventListener('click', function(){
        modalAddPlano.classList.toggle('active')    
    })
})

closeModalAdd.addEventListener('click', function(){
    modalAddPlano.classList.toggle('active')           
})


