let closeModa = document.querySelector('.modal-carrinho')
let openModalItens = Array.from(document.querySelectorAll('.detalhes-btn .btn'))
let body = document.querySelector('body');

if(closeModa){
    openModalItens.forEach(function(iten){
        iten.addEventListener('click', function(){
            body.classList.toggle('active')
            document.querySelector('.modal-carrinho').classList.toggle('active')
        })
    })
    
    closeModa.addEventListener('click', function(e){
        let click  = e.target.classList[0];
        if(click == 'modal-carrinho' || click == 'close-carrinho'){
            body.classList.toggle('active')
            closeModa.classList.toggle('active')
        }
    })
}