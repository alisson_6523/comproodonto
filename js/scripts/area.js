let itensNavArea = Array.from(document.querySelectorAll('#menu ul li'))
let listaArea = Array.from(document.querySelectorAll('.itens-menu'))
let listaCardsPlanos = Array.from(document.querySelectorAll('.card-plano'))
let btnModalAdd = document.querySelector('.btn-cadastro');
let menuMobile = document.querySelector('.menu-mobile')
let MenuDrop = document.querySelector('.menu-area')

function activeArea(areas){
    if(areas){
        areas.classList.add('active')
    }
}

function removeArea(item){

    if(item){
        itensNavArea.forEach(function(itensNavArea){
            if(item != itensNavArea){
                itensNavArea.classList.remove('active')
            }
        })
    }

    if(listaArea){
        listaArea.forEach(function(item){
            item.classList.remove('active')
        })
    }
}

itensNavArea.forEach(function(item){
    item.addEventListener('click', function(){
        let id = item.dataset.id

        let nomeItem = item.querySelector('p').innerHTML;

        document.querySelector('.target-plano p').innerHTML = nomeItem;

        item.classList.add('active')
        MenuDrop.classList.toggle('active')

        let itens = document.querySelector('#'+ id)

        removeArea(item)

        activeArea(itens)
    })
})

listaCardsPlanos.forEach(function(card){
    card.addEventListener('click', function(e){
        
        if((e.target.classList[0] != 'btn-add') && (e.target.classList[0] != 'modal')){
            let detalhePlano = document.querySelector('#detalhe-plano');

            document.querySelector('.target-plano p').innerHTML = "detalhe do plano";

            let navLisa = document.querySelector('#menu ul li[data-id=plano]')

            removeArea(navLisa)

            activeArea(detalhePlano)
        }

    })
})

btnModalAdd.addEventListener('click', function(e){
    e.preventDefault()
    

    let detalhePlano = document.querySelector('#detalhe-plano-beneficiario');

    let navLisa = document.querySelector('#menu ul li[data-id=plano]')

    document.querySelector('.target-plano p').innerHTML = 'detalhe do plano';

    document.querySelector('.modal-add-plano').classList.toggle('active')

    removeArea(navLisa)

    activeArea(detalhePlano)
})

menuMobile.addEventListener('click', function(){
    MenuDrop.classList.toggle('active')
})