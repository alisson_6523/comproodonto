// select home



let select = document.querySelector('.select')
let itensSelect = Array.from(document.querySelectorAll('.list li'))

if(select){
    select.addEventListener('click', function(){
        select.querySelector('.list').classList.toggle('active')
    })
    
    itensSelect.forEach(function(iten){
        iten.addEventListener('click', function(){
            //capturando data id
            let id = parseInt(iten.dataset.id)
    
            //set valor no input
            document.getElementById('cidade').value = id
    
            //set valor no select
            select.querySelector('span').innerHTML = iten.querySelector('p').innerHTML
        })
    })
}